﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using Breeze.ContextProvider;
using Breeze.ContextProvider.EF6;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using CC.Model;

namespace CC.DataAccess
{
    /// <summary>
    /// Repository (a "Unit of Work" really) of CodeCamper models.
    /// </summary>
    public class CodeCamperRepository
    {
        private readonly EFContextProvider<PodcastDbContext>
            _contextProvider = new EFContextProvider<PodcastDbContext>();

        private PodcastDbContext Context { get { return _contextProvider.Context; } }

        public string Metadata
        {
            get { return _contextProvider.Metadata(); }
        }

        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _contextProvider.SaveChanges(saveBundle);
        }

        public IQueryable<Session> Sessions
        {
            get { return Context.Sessions; }
        }

        public IQueryable<Person> Speakers
        {
            get { return Context.Persons.Where(p => p.SpeakerSessions.Any()); }
        }

        public IQueryable<Person> Persons
        {
            get { return Context.Persons; }
        }

        public IQueryable<Room> Rooms
        {
            get { return Context.Rooms; }
        }
        public IQueryable<TimeSlot> TimeSlots
        {
            get { return Context.TimeSlots; }
        }
        public IQueryable<Track> Tracks
        {
            get { return Context.Tracks; }
        }

        public IQueryable<UserSubscription> UserSubscriptions
        {
            get
            {
                string userId = HttpContext.Current.User.Identity.GetUserId();
                return
                    Context.UserSubscriptions.Where(u => u.UserId == userId);
            }
        }

        public SubscriptionRevision SubscriptionRevision
        {
            get
            {
                string userId = HttpContext.Current.User.Identity.GetUserId();
                return
                    Context.SubscriptionRevisions.FirstOrDefault(u => u.UserId == userId);
            }
        }
    }
}