﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CC.DataAccess
{
    public class AccountDbContext : IdentityDbContext
    {
        public AccountDbContext() : base(nameOrConnectionString: "CodeCamper") { }

    }
}
