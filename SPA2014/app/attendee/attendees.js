﻿(function () {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'attendees';

    // Define the controller on the module.
    // Inject the dependencies. 
    // Point to the controller definition function.
    angular.module('app').controller(controllerId,
        ['common','datacontext', attendees]);

    function attendees(common, datacontext) {
        // Using 'Controller As' syntax, so we assign this to the vm variable (for viewmodel).
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        // Bindable properties and functions are placed on vm.
        vm.title = 'Attendees';
        vm.attendees = [];
        vm.refresh = refresh;

        activate();
        function activate() {
            common.activateController([getAttendees()], controllerId)
            .then(function () { log('Activated Attendees View'); });
        }

        function getAttendees(forceRefresh) {
            return datacontext.getAttendees(forceRefresh).then(
                function(data) {
                    vm.attendees = data;
                    return data;
                });

        }

        function refresh() {
            getAttendees(true);
        }
    }
})();
