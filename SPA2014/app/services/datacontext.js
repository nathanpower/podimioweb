 (function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId,
        ['common', 'entityManagerFactory', 'model', 'config', datacontext]);

    function datacontext(common, emFactory, model, config) {
        var EntityQuery = breeze.EntityQuery;
        var entityNames = model.entityNames;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(serviceId);
        var logError = getLogFn(serviceId, 'error');
        var logSuccess = getLogFn(serviceId, 'success');
        var $q = common.$q;
        var manager = emFactory.newManager();
        var primePromise;

        

        var storeMeta = {
            isLoaded: {
                sessions: false,
                attendees: false
            }
        };




        var service = {
            processLogin: processLogin,
            getAttendees: getAttendees,
            getPeople: getPeople,
            getMessageCount: getMessageCount,
            getSessionPartials: getSessionPartials,
            getSpeakerPartials: getSpeakerPartials,
            getChannel: getChannel,
            prime: prime
        };

        return service;
        
        function processLogin(username, password) {
            var loggedIn = false;

            /*var token = localStorageSerice.get('podimio-token');
            if (token)
                return token;*/

            var credentials = { username: username, password: password }
            return $.ajax({
                type: "POST",
                url: "Token",
                data: JSON.stringify(credentials),
                dataType: 'json',
                contentType: "application/json",
                success: querySucceeded,
                fail: _queryFailed
            });
            
            
            function querySucceeded(data) {
                loggedIn = data.results;
                log('Retrieved [Successful Login] from remote data source', loggedIn, true);
                return loggedIn;
            }
        }

        
        function getChannel(query) {
            
            var channel;

            return EntityQuery.from('Channel')
            .withParameters({query: query})
            .using(manager).execute()
            .to$q(querySucceeded, _queryFailed);
            
            function querySucceeded(data) {
                channel = data.results;
                log('Retrieved [Channel] from remote data source', channel.length, true);
                return channel;
            }
        }

        function getMessageCount() { return $q.when(72); }

        function getPeople() {
            var people = [
                { firstName: 'John', lastName: 'Papa', age: 25, location: 'Florida' },
                { firstName: 'Ward', lastName: 'Bell', age: 31, location: 'California' },
                { firstName: 'Colleen', lastName: 'Jones', age: 21, location: 'New York' },
                { firstName: 'Madelyn', lastName: 'Green', age: 18, location: 'North Dakota' },
                { firstName: 'Ella', lastName: 'Jobs', age: 18, location: 'South Dakota' },
                { firstName: 'Landon', lastName: 'Gates', age: 11, location: 'South Carolina' },
                { firstName: 'Haley', lastName: 'Guthrie', age: 35, location: 'Wyoming' }
            ];
            return $q.when(people);
        }
        
        function getAttendees(forceRemote) {
            var attendeeOrderBy = 'firstName, lastName';
            var attendees = [];
            
            if (_areAttendeesLoaded() && !forceRemote) {
                attendees = _getAllLocal(entityNames.attendee, attendeeOrderBy);
                return $q.when(attendees);
            }

            return EntityQuery.from('Persons')
            .select('id, firstName, lastName, imageSource')
            .orderBy(attendeeOrderBy)
            .toType(entityNames.attendee)
            .using(manager).execute()
            .to$q(querySucceeded, _queryFailed);

            function querySucceeded(data) {
                attendees = data.results;
                _areAttendeesLoaded(true);
                log('Retrieved [Attendee] from remote data source', attendees.length, true);
                return attendees;
            }
        }

        function getSpeakerPartials(forceRemote) {
            var predicate = breeze.Predicate.create('isSpeaker', '==', true)
            var speakerOrderBy = 'firstName, lastName';
            var speakers = [];

            if (!forceRemote) {
                speakers = _getAllLocal(entityNames.speaker, speakerOrderBy, predicate);
                return $q.when(speakers);
            }
            
            return EntityQuery.from('Speakers')
            .select('id, firstName, lastName, imageSource')
            .orderBy(speakerOrderBy)
            .toType(entityNames.speaker)
            .using(manager).execute()
            .to$q(querySucceeded, _queryFailed);
            
            function querySucceeded(data) {
                speakers = data.results;
                for (var i = speakers.length; i--;) {
                    speakers[i].isSpeaker = true;
                }
                log('Retrieved [Speaker Partials] from remote data source',  speakers.length, true);
                return speakers;
            }
        }

        function getSessionPartials(forceRemote) {
            var orderBy = 'timeSlotId, level, speaker.firstName';
            var sessions;         

            if (_areSessionsLoaded() && !forceRemote) {
                sessions = _getAllLocal(entityNames.session, orderBy);
                return $q.when(sessions);
            }

            return EntityQuery.from('Sessions')
                .select('id, title, code, speaker, trackId, timeSlotId, roomId, level, tags')
                .orderBy(orderBy)
                .toType(entityNames.session)
                .using(manager).execute()
                .to$q(querySucceeded, _queryFailed);
            
            function querySucceeded(data) {
                sessions = data.results;
                _areSessionsLoaded(true);
                log('Retrieved [Session Partials] from remote data source', sessions.length, true);
                return sessions;
            }
            
           
        }

        function prime() {

            if (primePromise) return primePromise;

            primePromise = $q.all([getLookups(), getSpeakerPartials(true)])
                .then(extendMetaData)
                .then(success);
            
            return primePromise;

            function success() {
                setLookups();
                log('Primed the data');
            }

            function extendMetaData() {
                var metaDataStore = manager.metadataStore;
                var types = metaDataStore.getEntityTypes();
                types.forEach(function(type) {
                    if (type instanceof breeze.EntityType) {
                        set(type.shortName, type);
                    }
                });


                var personEntityName = entityNames.person;
                ['Speakers', 'Speaker', 'Attendees', 'Attendee'].forEach(function(r) {
                    console.info(r + ' : ' + personEntityName);
                    set(r, personEntityName);
                });

                function set(resourceName, entityName) {
                    metaDataStore.setEntityTypeForResourceName(resourceName, entityName);
                }
            }

        }

        function setLookups() {
            
            service.lookupCachedData = {
                rooms: _getAllLocal(entityNames.room, 'name'),
                tracks: _getAllLocal(entityNames.track, 'name'),
                timeslots: _getAllLocal(entityNames.timeslot, 'start')
            };
        }
        
        function _getAllLocal(resource, ordering, predicate) {
            return EntityQuery.from(resource)
                .orderBy(ordering)
                .where(predicate)
                .using(manager)
                .executeLocally();
        }

        function getLookups() {
            return EntityQuery.from('Lookups')
                .using(manager).execute()
                .to$q(querySucceeded, _queryFailed);
            
            function querySucceeded(data) {
                log('Retrieved [Lookups] from remote data source', data, true);
                return true;
            }
        }
        
        function _queryFailed(error) {
            var msg = config.appErrorPrefix + 'Error retreiving data' + error.message;
            logError(msg, error);
            throw error;
        }
        
        function _areSessionsLoaded(value) {
            return _areItemsLoaded('sessions', value);
        }
        
        function _areAttendeesLoaded(value) {
            return _areItemsLoaded('attendees', value);
        }
        
        function _areItemsLoaded(key, value) {
            if (value === undefined) {
                return storeMeta.isLoaded[key]; // get
            }
            return storeMeta.isLoaded[key] = value; // set
        }
    }
})();