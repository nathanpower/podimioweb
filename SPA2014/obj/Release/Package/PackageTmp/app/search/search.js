﻿(function () {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'search';

    // Define the controller on the module.
    // Inject the dependencies. 
    // Point to the controller definition function.
    angular.module('app').controller(controllerId,
        ['common', 'datacontext', search]);

    function search(common, datacontext) {
        // Using 'Controller As' syntax, so we assign this to the vm variable (for viewmodel).
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        // Bindable properties and functions are placed on vm.
        vm.title = 'Search';
        vm.query = '';
        vm.results = [];
        vm.getChannel = getChannel;

        activate();

        function activate() {
            common.activateController([], controllerId)
               .then(function () { log('Activated Search View'); });
        }
        
        function getChannel() {
            return datacontext.getChannel(vm.query).then(function (data) {
                return vm.results = data;
            });
        }
        

        //#region Internal Methods        

        //#endregion
    }
})();
