﻿(function () {
    'use strict';

    // Factory name is handy for logging
    var serviceId = 'model';

    // Define the factory on the module.
    // Inject the dependencies. 
    // Point to the factory definition function.
    // TODO: replace app with your module name
    angular.module('app').factory(serviceId, model);

    function model($http) {
        // Define the functions and properties to reveal.

        var entityNames = {
            attendee: 'Person',
            person: 'Person',
            speaker: 'Person',
            session: 'Session',
            room: 'Room',
            track: 'Track',
            timeslot: 'TimeSlot'
        };
        
        var service = {
            configureMetaDataStore: configureMetaDataStore,
            entityNames: entityNames
        };

        return service;

        function configureMetaDataStore(metadataStore) {
            registerTimeSlot(metadataStore);
            registerSession(metadataStore);
            registerPerson(metadataStore);
        }

        //#region Internal Methods      
        
        function registerSession(metadataStore) {
            metadataStore.registerEntityTypeCtor('Session', Session);
            
            function Session() { }

            Object.defineProperty(Session.prototype, 'tagsFormatted', {
                get: function() {
                    return this.tags ? this.tags.replace(/\|/g, ', ') : this.tags;
                },
                set: function() {
                    this.tags = value.replace(/\, /g, '|');
                }
            });
        }

        function registerTimeSlot(metadataStore) {
            metadataStore.registerEntityTypeCtor('TimeSlot', TimeSlot);

            function TimeSlot() { }

            Object.defineProperty(TimeSlot.prototype, 'name', {
                get: function() {
                    // Format date
                    var start = this.start;
                    var value = moment.utc(start).format('ddd hh:mm a');
                    return value;
                }
            });
        }
        
        function registerPerson(metadataStore) {
            metadataStore.registerEntityTypeCtor('Person', Person);

            function Person() {
                this.isSpeaker = false; 
            }

            Object.defineProperty(Person.prototype, 'fullName', {                
                get: function() {
                    return this.fullName + ' ' + this.lastName;
                }
            });
            

        }
            

        //#endregion
    }
})();