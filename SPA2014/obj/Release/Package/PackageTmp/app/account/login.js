﻿(function () {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'login';

    // Define the controller on the module.
    // Inject the dependencies. 
    // Point to the controller definition function.
    angular.module('app').controller(controllerId,
        ['common', 'datacontext', login]);

    function login(common, datacontext) {
        // Using 'Controller As' syntax, so we assign this to the vm variable (for viewmodel).
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var _tokenKey = 'podimio-token';

        // Bindable properties and functions are placed on vm.
        vm.title = 'Login';
        vm.username = '';
        vm.password = '';
        vm.processLogin = processLogin;
        vm.token = '';
        vm.loggedIn = 'false';

        activate();

        function activate() {
            common.activateController([], controllerId)
               .then(function () {
                   
                   if (tokenExistsInLocalStorage(_tokenKey))
                       vm.loggedIn = true;
                   
                   log('Activated Login View');
               });
        }

        function processLogin() {
            return datacontext.processLogin(vm.username, vm.password).then(function (data) {
                return vm.token = data;
            });
            
        }
        
        /*function tokenExistsInLocalStorage(tokenKey) {
            if (localStorageService.get(tokenKey)) return true;
            return false;
        }*/


        //#region Internal Methods        

            //#endregion
        }
})();
