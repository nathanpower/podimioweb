﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CC.Core;
using CC.DataAccess;
using CC.Model;
using Google.Apis.Customsearch.v1.Data;
using Microsoft.AspNet.Identity;
using SPA2014.Attributes;

namespace SPA2014.Controllers
{
    public class SubscriptionsController : ApiController
    {
        // Todo: inject via an interface rather than "new" the concrete class
        readonly PodcastDbContext _db = new PodcastDbContext();

        [HttpGet]
        [AuthorizeUser]
        public Object Revision()
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            var revision = _db.SubscriptionRevisions.FirstOrDefault(u => u.UserId == userId);

            return Request.CreateResponse(HttpStatusCode.OK, revision);
        }

        [HttpPost]
        [AuthorizeUser]
        public Object Add([FromBody] string channelUrl)
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            var subscription = new UserSubscription()
            {
                UserId = userId,
                ChannelUrl = channelUrl
            };

            var revision = _db.SubscriptionRevisions.First(r => r.UserId == userId);

            _db.UserSubscriptions.Add(subscription);
            revision.RevisionId = Guid.NewGuid().ToString(); 
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, revision);
        }

        [HttpPost]
        [AuthorizeUser]
        public Object Delete([FromBody] string channelUrl)
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();

            var revision = _db.SubscriptionRevisions.First(r => r.UserId == userId);
            var subscription =
                _db.UserSubscriptions.FirstOrDefault(u => u.UserId == userId && u.ChannelUrl == channelUrl);

            _db.UserSubscriptions.Remove(subscription);
            revision.RevisionId = Guid.NewGuid().ToString(); 
            _db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, revision);
        }

        [HttpGet]
        [AuthorizeUser]
        public Object All()
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();

            var subscriptions =
                _db.UserSubscriptions.Where(u => u.UserId == userId).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, subscriptions);
        }

        [HttpGet]
        [AuthorizeUser]
        public Object RefreshChannel(string channelUrl, string episodeUrl)
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            Channel channel = null;
            IFeedParser parser = new PodcastParser();

            channel = parser.ParseChannel(channelUrl, new PodcastDefinition());

            if(channel.Items.ElementAt(0).Url != episodeUrl)
                return Request.CreateResponse(HttpStatusCode.OK, channel);

            else return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        [HttpGet]
        //[AuthorizeUser]
        public Object Search(string query)
        {
            Channel channel = null;
            IFeedProvider search = new PodcastSearchEngine();
            IFeedParser parser = new PodcastParser();

            var results = search.GetChannel(query);

            foreach (Result item in results)
            {
                channel = parser.ParseChannel(item.Link, new PodcastDefinition());
                if (channel != null) break;
            }

            return Request.CreateResponse(HttpStatusCode.OK, channel);
        }

        [HttpGet]
        [AuthorizeUser]
        public Object Channel(string url)
        {
            IFeedParser parser = new PodcastParser();
            Channel channel = parser.ParseChannel(url, new PodcastDefinition());
            return Request.CreateResponse(HttpStatusCode.OK, channel);
        }

    }
}
