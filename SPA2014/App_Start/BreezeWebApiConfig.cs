using System.Net.Http.Headers;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using SPA2014;

[assembly: WebActivator.PreApplicationStartMethod(
    typeof(SPA2014.App_Start.BreezeWebApiConfig), "RegisterBreezePreStart")]
namespace SPA2014.App_Start {
  ///<summary>
  /// Inserts the Breeze Web API controller route at the front of all Web API routes
  ///</summary>
  ///<remarks>
  /// This class is discovered and run during startup; see
  /// http://blogs.msdn.com/b/davidebb/archive/2010/10/11/light-up-your-nupacks-with-startup-code-and-webactivator.aspx
  ///</remarks>
  public static class BreezeWebApiConfig {

      public static void RegisterBreezePreStart()
      {
          HttpConfiguration config = GlobalConfiguration.Configuration;

          config.Routes.MapHttpRoute(
          name: "BreezeApi",
          routeTemplate: "breeze/{controller}/{action}"
          );
          config.SuppressDefaultHostAuthentication();
          config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
          // Web API configuration and services
          config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
          // Web API routes

          config.MapHttpAttributeRoutes();

          config.Routes.MapHttpRoute(
              name: "AccountApi",
              routeTemplate: "api/{controller}/{id}",
              defaults: new { id = RouteParameter.Optional }
          );
    }


  }
}