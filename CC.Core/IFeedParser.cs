﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CC.Model;


namespace CC.Core
{
    public interface IFeedParser
    {
        Channel ParseChannel(string url, IFeedDefinition feedDefinition);
    }
}
