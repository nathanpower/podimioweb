﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;
using Google.Apis.Services;

namespace CC.Core
{
    public class PodcastSearchEngine : IFeedProvider
    {
        public IEnumerable GetChannel(string channelName)
        {
            Search search = null;
            string apiKey = ConfigurationManager.AppSettings.Get("GoogleSearchApiKey");
            string cseKey = ConfigurationManager.AppSettings.Get("CustomSearchEngineKey");
            var bcsi = new BaseClientService.Initializer { ApiKey = apiKey };
            var css = new CustomsearchService(bcsi);
            var listRequest = css.Cse.List(channelName + "feed");
            listRequest.Cx = cseKey;
            try
            {
                search = listRequest.Execute();
            }
            catch (Exception e)
            {
                throw new Exception("Search not available");
            }
            return search.Items;
        }
    }
}