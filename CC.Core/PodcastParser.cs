﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.ServiceModel.Syndication;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using CC.Model;
using HtmlAgilityPack;


namespace CC.Core
{
    public class PodcastParser : IFeedParser
    {
        public Channel ParseChannel(string url, IFeedDefinition feedDefinition)
        {
            byte[] responseData = null;
            byte [] feedResponseData = null;
            if (String.IsNullOrEmpty(url))
                return null;

            try
            {
                HttpWebResponse response = GetResponse(url);
                responseData = GetResponseData(response);
                if (SiteIsHtml(response))
                {                  
                    string feedUrl = GetFeedUrlIfAvailable(responseData, url);
                    if (feedUrl != null) feedResponseData = GetFeedResponseIfValid(feedUrl);
                    if (feedResponseData != null)
                    {
                        url = feedUrl;
                        responseData = feedResponseData;
                    }

                }
                    
                Channel feed = ReadFromFeed(url, responseData, feedDefinition);
                return feed;
            }

            catch (Exception e)
            {
                return null;
            }
        }


        private HttpWebResponse GetResponse(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            return response;
        }

        private byte[] GetResponseData(HttpWebResponse response)
        {
            Stream stream = response.GetResponseStream();
            var ms = new MemoryStream();
            stream.CopyTo(ms);
            response.Close();
            byte[] data = ms.ToArray();
            return data;
        }

        private Stream GetStreamFromResponseData(byte[] data)
        {
            Stream stream = new MemoryStream(data);
            return stream;
        }


        private bool SiteIsHtml(HttpWebResponse response)
        {
            var responseType = response.ContentType;
            return responseType.Contains("text/html");
        }

        private string GetFeedUrlIfAvailable(byte[] responseData, string url)
        {
            Stream responseStream = GetStreamFromResponseData(responseData);
            var doc = new HtmlDocument();
            doc.Load(responseStream);
            HtmlNodeCollection linkNodes = doc.DocumentNode.SelectNodes("//link");

            foreach (HtmlNode node in linkNodes)
            {
                var attributes = node.Attributes;
                if (attributes != null && attributes["type"] != null && attributes["href"] != null)
                {
                    if (attributes["type"].Value == "application/rss+xml" ||
                        attributes["type"].Value == "application/atom+xml")
                    {
                        string result = node.Attributes["href"].Value;
                        if (result == url) return null;
                        else return result;
                    }
                }

            }
            return null;
        }


        private byte[] GetFeedResponseIfValid(string url)
        {

            HttpWebResponse response = GetResponse(url);
            byte[] responseData = GetResponseData(response);
            Stream responseStream = GetStreamFromResponseData(responseData);

            using (XmlReader reader = XmlReader.Create(responseStream, GetReaderSettings()))
            {
                if (reader.ReadState == ReadState.Initial)
                    reader.MoveToContent();

                var atom = new Atom10FeedFormatter();
                if (atom.CanRead(reader))
                {
                    return responseData;
                }

                var rss = new Rss20FeedFormatter();
                if (rss.CanRead(reader))
                {
                    return responseData;
                }
            }
            return null;
        }

        private Channel ReadFromFeed(string url, byte[] response, IFeedDefinition feedDefinition)
        {
            var stream = GetStreamFromResponseData(response);
            
            using (XmlReader reader = XmlReader.Create(stream, GetReaderSettings()))
            {
                if (reader.ReadState == ReadState.Initial)
                    reader.MoveToContent();

                var atom = new Atom10FeedFormatter();
                if (atom.CanRead(reader))
                {
                    var atomStream = GetStreamFromResponseData(response);
                    var channels = GetAtomChannels(XDocument.Load(atomStream), url).ToArray();
                    if (channels.Length != 1) throw new Exception();
                    if (IsAudioFeed(feedDefinition, channels)) return channels[0];
                    else return null;
                }

                var rss = new Rss20FeedFormatter();
                if (rss.CanRead(reader))
                {
                    var rssStream = GetStreamFromResponseData(response);
                    var channels = GetRssChannels(XDocument.Load(rssStream), url).ToArray();
                    if (IsAudioFeed(feedDefinition, channels)) return channels[0];
                    else return null;
                }
            }
            return null;
        }

        private bool IsAudioFeed(IFeedDefinition feedDefinition, Channel[] channels)
        {
            return feedDefinition.GetValidFileExtensions()
                .Contains(Path.GetExtension(channels[0].Items.First().Url), 
                StringComparer.OrdinalIgnoreCase);

        }

        private XmlReaderSettings GetReaderSettings()
        {
            return new XmlReaderSettings
            {
                IgnoreWhitespace = true,
                CheckCharacters = true,
                CloseInput = true,
                IgnoreComments = true,
                IgnoreProcessingInstructions = true,
                DtdProcessing = DtdProcessing.Ignore
            };
        }

        public IEnumerable<Channel> GetRssChannels(XDocument xdoc, string url)
        {
            return from channels in xdoc.Descendants("channel")
                   select new Channel
                   {
                       Title = channels.Element("title") != null ? channels.Element("title").Value : "",
                       Url = url,
                       Description = channels.Element("description") != null ? channels.Element("description").Value : "",
                       Items = from items in channels.Descendants("item")
                               select new Item
                               {
                                   Title = items.Element("title") != null ? items.Element("title").Value : "",
                                   Url = items.Element("enclosure") != null ? items.Element("enclosure").Attribute("url").Value : "",
                                   Description = items.Element("description") != null ? items.Element("description").Value : "",
                                   Guid = (items.Element("guid") != null ? items.Element("guid").Value : "")
                               }
                   };
        }

        public IEnumerable<Channel> GetAtomChannels(XDocument xdoc, string url)
        {
            return from channels in xdoc.Descendants("feed")
                   select new Channel
                   {
                       Title = channels.Element("title") != null ? channels.Element("title").Value : "",
                       Url = url,
                       Description = channels.Element("subtitle") != null ? channels.Element("subtitle").Value : "",
                       Items = from items in channels.Descendants("entry")
                               select new Item
                               {
                                   Title = items.Element("title") != null ? items.Element("title").Value : "",
                                   Url = items.Element("link") != null ? items.Element("link").Value : "",
                                   Description = items.Element("summary") != null ? items.Element("summary").Value : "",
                                   Guid = (items.Element("id") != null ? items.Element("id").Value : "")
                               }
                   };
        }
    }
}