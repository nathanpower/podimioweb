﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CC.Core
{
    public class PodcastDefinition : IFeedDefinition
    {
        public string[] GetValidFileExtensions()
        {
            string[] audioExtensions = {
                ".WAV", ".WMA", ".MP3", ".OGG", ".RMA", ".FLAC"
            };

            return audioExtensions;
        }
    }
}