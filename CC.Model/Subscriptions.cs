﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CC.Model
{
    [DataContract]
    public class Subscriptions
    {
        [DataMember]
        public SubscriptionRevision SubscriptionRevision { get; set; }

        [DataMember]
        public List<UserSubscription> ChannelUrls { get; set; }
    }
}
