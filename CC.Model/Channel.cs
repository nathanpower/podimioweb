﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Policy;
using System.Web;

namespace CC.Model
{
    [DataContract]
    public class Channel
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public IEnumerable<Item> Items { get; set; }

        [DataMember]
        public string Url { get; set; }

    }
}