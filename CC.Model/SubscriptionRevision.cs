﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CC.Model
{
    [DataContract]
    public class SubscriptionRevision
    {
        [Key]
        public String UserId { get; set; }

        [DataMember]
        public String  RevisionId { get; set; }
    }
}
